#include <boost/assign.hpp>
#include "mouse_racer_driver/jackal_hardware.h"

namespace mouse_racer_driver
{

  MouseRacerHardware::MouseRacerHardware(std::string left_joint_name, std::string right_joint_name)
  {

    hardware_interface::JointStateHandle joint_state_handle_left(left_joint_name,
                                                                 &left_joint.position, &left_joint.velocity, &left_joint.effort);
    jnt_state_interface.registerHandle(joint_state_handle);

    hardware_interface::JointStateHandle joint_state_handle_right(right_joint_name,
                                                                  &right_joint.position, &right_joint.velocity, &right_joint.effort);
    jnt_state_interface.registerHandle(joint_state_handle);

    registerInterface(&jnt_state_interface);
    registerInterface(&velocity_joint_interface_);

    feedback_sub_ = nh_.subscribe("feedback", 1, &JackalHardware::feedbackCallback, this);

    // Realtime publisher, initializes differently from regular ros::Publisher
    cmd_drive_pub_.init(nh_, "cmd_drive", 1);
  }

  /**
   * Estimates joint states based on velocity command.
   *
   * Called from the controller thread.
   */
  void MouseRacerHardware::copyJointsFromHardware()
  {
    boost::mutex::scoped_lock feedback_msg_lock(feedback_msg_mutex_, boost::try_to_lock);
    if (feedback_msg_ && feedback_msg_lock)
    {
      left_joint.position = feedback_msg_->position_left;
      right_joint.position = feedback_msg_->position_right;
      left_joint.velocity = feedback_msg_->velocity_left;
      right_joint.velocity = feedback_msg_->velocity_right;
      left_joint.effort = 0;
      right_joint.effort = 0;
    }
  }

  /**
   * Populates and publishes Drive message based on the controller outputs.
   *
   * Called from the controller thread.
   */
  void MouseRacerHardware::publishDriveFromController()
  {
    if (cmd_drive_pub_.trylock())
    {
      cmd_drive_pub_.msg_.mode = mouse_racer_msgs::Drive::MODE_VELOCITY;
      cmd_drive_pub_.msg_.cmd_left = left_joint.command;
      cmd_drive_pub_.msg_.cmd_right = right_joint.command;
      cmd_drive_pub_.unlockAndPublish();
    }
  }

  void JackalHardware::feedbackCallback(const jackal_msgs::Feedback::ConstPtr &msg)
  {
    // Update the feedback message pointer to point to the current message. Block
    // until the control thread is not using the lock.
    boost::mutex::scoped_lock lock(feedback_msg_mutex_);
    feedback_msg_ = msg;
  }

} // namespace mouse_racer_driver
