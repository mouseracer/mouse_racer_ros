#pragma once

#include "boost/thread.hpp"
#include "hardware_interface/joint_state_interface.h"
#include "hardware_interface/joint_command_interface.h"
#include "hardware_interface/robot_hw.h"
#include "mouse_racer_msgs/Drive.h"
#include "mouse_racer_msgs/Feedback.h"
#include "realtime_tools/realtime_publisher.h"
#include "ros/ros.h"
#include "sensor_msgs/JointState.h"

namespace mouse_racer_driver
{
  struct Joint
  {
    double position, velocity, effort, command;
  };

  class MouseRacerHardware : public hardware_interface::RobotHW
  {
  public:
    MouseRacerHardware();
    void copyJointsFromHardware();
    void publishDriveFromController();

  private:
    void feedbackCallback(const mouse_racer_msgs::Feedback::ConstPtr &msg);

    ros::NodeHandle nh_;
    ros::Subscriber feedback_sub_;
    realtime_tools::RealtimePublisher<mouse_racer_msgs::Drive> cmd_drive_pub_;

    // Define rosserial interfaces
    hardware_interface::JointStateInterface jnt_state_interface;
    hardware_interface::VelocityJointInterface velocity_joint_interface_;

    // Define joints
    Joint left_joint;
    Joint right_joint;

    // This pointer is set from the ROS thread.
    mouse_racer_ros::Feedback::ConstPtr feedback_msg_;
    boost::mutex feedback_msg_mutex_;
  };

}
