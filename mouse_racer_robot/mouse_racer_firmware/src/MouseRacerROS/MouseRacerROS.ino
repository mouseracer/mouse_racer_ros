/////////////////////////////////////////////////
//
// MouseRacerROS firmware for ESP8266
//
// Find source code at:
// https://gitlab.com/mouseracer
//
// Inspired by Agustin Nunez, find original
// code at:
// https://github.com/agnunez/espros.git
//
// MIT License 2022, Julien Kindle, Agustin Nunez
/////////////////////////////////////////////////
#include "WiFiConfiguration.h"
#include <ESP8266WiFi.h>
#include <ros.h>
#include <geometry_msgs/Twist.h>
#include <SparkFun_TB6612.h>

// Pins for all inputs
#define AIN1 D5
#define BIN1 D6
#define AIN2 D4
#define BIN2 D7
#define PWMA D2
#define PWMB D8
#define STBY D3

// these constants are used to allow you to make your motor configuration 
// line up with function names like forward.  Value can be 1 or -1
const int offsetL = -1;
const int offsetR = -1;

// Defining parameters
float velocity_conversion_factor; // (rad/s) per int8
float wheel_radius;
float baseline;

// ROS communication
ros::NodeHandle nh;
void cmdVelCallback(const geometry_msgs::Twist& msg);
ros::Subscriber<geometry_msgs::Twist> sub_cmdvel("/mouseracer/cmd_vel", &cmdVelCallback);

// Initializing motors
Motor motor_r = Motor(AIN1, AIN2, PWMA, offsetL, STBY);
Motor motor_l = Motor(BIN1, BIN2, PWMB, offsetR, STBY);

// WiFi setup function
void setupWiFi() {
  Serial.println("Connecting to wifi...");
  WiFi.begin(WIFISSID, WIFIPASSWORD);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

// Callback function for subscription
void cmdVelCallback(const geometry_msgs::Twist& msg) {
  double v_l = msg.linear.x - msg.angular.z * baseline/2.0;
  double v_r = msg.linear.x + msg.angular.z * baseline/2.0;

  int motor_left_cmd = int(min(255.0, max(-255.0, v_l/wheel_radius / velocity_conversion_factor)));
  int motor_right_cmd = int(min(255.0, max(-255.0, v_r/wheel_radius / velocity_conversion_factor)));
  Serial.print("LEFT "); Serial.println(motor_left_cmd);
  Serial.print("RIGHT "); Serial.println(motor_right_cmd);
  motor_l.drive(motor_left_cmd);
  delay(1);
  motor_r.drive(motor_right_cmd);
}

// Start of program
void setup() {
  // Start serial for debugging
  Serial.begin(115200);

  // Initialize WiFi Connection
  setupWiFi();
  delay(2000);

  // Set up rosserial connection
  Serial.println("Setting up rosserial");
  IPAddress server(ROSSERIALIP);
  nh.getHardware()->setConnection(server, ROSSERIALPORT);

  // Initialize node
  Serial.println("Init node");
  nh.initNode();
  while (!nh.connected()) {
        nh.spinOnce();
  }

  // Load parameters
  Serial.println("Loading parameters");
  if (!nh.getParam("~velocity_conversion_factor", &velocity_conversion_factor)) {
    velocity_conversion_factor = 0.00946262847;
  }
  if (!nh.getParam("~wheel_radius", &wheel_radius)) {
    wheel_radius = 0.0115;
  }
  if (!nh.getParam("~baseline", &baseline)) {
    baseline = 0.033;
  }

  // Set up subscription
  Serial.println("Starting subscription");
  nh.subscribe(sub_cmdvel);
}

// Main loop
void loop() {
  nh.spinOnce();
  delay(1);
}
